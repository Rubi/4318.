# Nanogame Template

This template is intended to be used as a base to develop community nanogames
for [Librerama](https://codeberg.org/Yeldham/librerama). It comes with all the
project/export settings properly configured and contains all the basic files
necessary, as well as the their correct structure.

Instructions on how to use this template to develop a nanogame can be found in
[this wiki page](https://codeberg.org/Yeldham/librerama/wiki/Developing-a-Community-Nanogame)
within the main repository.
